import React, { Component } from 'react';
import './css/pure-min.css';
import './css/side-menu.css'
import http from './http'
import Input from './components/input'

class App extends Component {

  state = {
    lista: [],
    nome: '',
    email: '',
    senha: ''
  }


  carregarLista(){
    // criei o http para deixar o axios a parte
    http.get('/api/autores').then(response => {
      this.setState({ lista:response.data })
      this.setState({
          nome: '',
          email: '',
          senha: ''
      })
    })
  }

  componentWillMount(){
    this.carregarLista()
  }

  enviaForm(event){
    event.preventDefault()
    // PEGA TODOS MENOS A LISTA
    const { lista, ...other } = this.state
    http.post('/api/autores', { ...other })
      .then(() => this.carregarLista())
      .catch(err => { console.log('Erro ao gravar') })
  }

  render() {

    const { nome, email, senha, lista } = this.state
    return (
      <div id="layout">
          <a href="#menu" id="menuLink" className="menu-link">
            <span></span>
          </a>

          <div id="menu">
              <div className="pure-menu">
                <a className="pure-menu-heading" href="#">Biblioteca</a>

                <ul className="pure-menu-list">
                  <li className="pure-menu-item"><a href="#" className="pure-menu-link">Home</a></li>
                  <li className="pure-menu-item"><a href="#" className="pure-menu-link">Autor</a></li>
                  <li className="pure-menu-item"><a href="#" className="pure-menu-link">Livro</a></li>
                </ul>
              </div>
          </div>

          <div id="main">
            <div className="header">
              <h1>Cadastro de autores</h1>
            </div>

            <div className="content" id="content">
              <div className="pure-form pure-form-aligned">
                <form className="pure-form pure-form-aligned" onSubmit={ e => this.enviaForm(e) }>
                  <div className="pure-control-group">
                    <Input id="nome" value={ nome } onChange={e => this.setState({ nome: e.target.value })} type="text" name="Nome" label="Nome"/>
                  </div>
                  <div className="pure-control-group">
                    <Input id="nome" value={ email } onChange={e => this.setState({ email: e.target.value })} type="email" name="Email" label="E-mail"/>
                  </div>
                  <div className="pure-control-group">
                    <Input id="senha" value={ senha } onChange={e => this.setState({ senha: e.target.value })} type="password" name="Nome" label="Senha"/>
                  </div>
                  <div className="pure-control-group">
                    <label></label>
                    <button type="submit" className="pure-button pure-button-primary">Gravar</button>
                  </div>
                </form>
              </div>

              <div>
                <table className="pure-table">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>email</th>
                      <th>Senha</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      lista.map((autor, key) => {
                        return(
                          <tr key={ autor.id }>
                            <td>{ autor.nome }</td>
                            <td>{ autor.email }</td>
                            <td>{ autor.senha }</td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
    )
  }
}

export default App;
