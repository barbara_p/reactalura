import React from 'react'


export default class Input extends React.Component{
  render(){
    return(
      <div className="pure-control-group">
      <label htmlFor={ this.props.id }>{ this.props.label }</label>
      <input
        id={ this.props.id }
        autoComplete="off"
        value={ this.props.value }
        onChange={ e => this.props.onChange(e) }
        type={ this.props.type }
        name={ this.props.name } />
    </div>
    )
  }
}