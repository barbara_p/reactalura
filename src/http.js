import axios from 'axios'

const http = axios.create({
  baseURL: 'http://cdc-react.herokuapp.com/'
})


export default http